module.exports = function(grunt){

	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        jekyll: {
            options: {
                bundleExec: false
            },
            build: {
                options: {
                    config: '_config.yml'
                }
            }
        },
        htmlhint: {
		    build: {
		        options: {
		            'tag-pair': true,
		            'tagname-lowercase': true,
		            'attr-lowercase': true,
		            'attr-value-double-quotes': true,
		            'doctype-first': true,
		            'spec-char-escape': true,
		            'id-unique': true,
		            'style-disabled': true
		        },
		        src: ['build/*.html']
		    }
		},
		sass: {                              
		    build: {
		    	options: {                       
					style: 'compressed'
				},
				files: {
					'build/assets/css/styles.css': 'assets/_sass/main.scss'
				}
		    }
  		},
		autoprefixer: {
		    single_file: {		 
		      src: 'build/assets/css/styles.css',
		      dest: 'build/assets/css/styles.css'
		    }
	  	},
		concat: {
	        js : {
	            src : [ 'assets/_bower_components/jquery/dist/jquery.js', 'assets/_js/scripts.js'],
	            dest : 'build/assets/js/scripts.js'
	        }
	    },
	    uglify: {
		    build: {
		        files: {
		            'build/assets/js/scripts.js': ['build/assets/js/scripts.js'],
		            'build/assets/js/modernizr.js': ['assets/_bower_components/modernizr/modernizr.js']
		        }
		    }
		},
		imagemin: {                          
		    dynamic: {                         
				options: {                       
					optimizationLevel: 7
				},
				files: [{
					expand: true,                  
					cwd: 'build/assets/img/',
					src: [ '**/*' ], 
					dest: 'build/assets/img/'            
				}]
		    }
	  	},
		watch: {
            scss: {
                files: [ 'assets/_sass/**/*.scss' ],
                tasks: [ 'sass', 'autoprefixer', 'notify:watch' ]
            },
            js: {
                files: [ 'assets/_js/*.js' ],
                tasks: [ 'concat', 'uglify', 'notify:watch' ]
            },
            html: {
                files: [ '*.html', '_includes/*.html' ],
                tasks: [ 'jekyll:build', 'htmlhint', 'sass', 'autoprefixer', 'concat', 'uglify', 'notify:watch' ]
            },
            options: {
		        livereload: true,
		    }
        },
        notify: {
			build: {
				options: {
					title: 'Site build',
					message: 'Build complete'
				}
			},
			watch: {
				options: {
					title: 'Success',
					message: 'File/s built'
				}
			}
	    }
    });

	grunt.registerTask( 'genesis', [
        'jekyll',
        'htmlhint',
        'sass',
        'autoprefixer',
        'concat',
        'uglify',
        'imagemin',
        'notify:build'
    ]);

    grunt.registerTask( 'images', [ 'imagemin' ] );

};